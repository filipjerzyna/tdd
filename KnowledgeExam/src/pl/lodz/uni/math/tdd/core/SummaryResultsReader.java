package pl.lodz.uni.math.tdd.core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SummaryResultsReader {
	private String file;

	public void setFile(String file) {
		this.file = file;
	}

	public SummaryResultsReader(String file) {
		this.file = file;
	}

	public Boolean fileExists() {

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (IOException e) {
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e2) {
					e2.printStackTrace();
					return false;
				}
			}
		}

		return true;
	}

	public List<Double> readResults() {
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ";";
		List<Double> results = new ArrayList<>();
		try {

			br = new BufferedReader(new FileReader(file));
			while ((line = br.readLine()) != null) {
				String[] splittedResult = line.split(cvsSplitBy);
				results.add(new Double(splittedResult[1]));
			}

		} catch (FileNotFoundException e) {

			return null;
		} catch (IOException e) {

			return null;
		} catch (Exception e) {

			return null;

		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {

					return null;
				}
			}
		}

		return results;

	}
}
