package pl.lodz.uni.math.tdd.core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AnswersReader {

	public void setAnswersPath(String properAnswersPath) {
		this.answersPath = properAnswersPath;
	}

	private String answersPath;
	private List<String> answers;

	public List<String> getAnswers() {
		return answers;
	}

	public void setAnswers(List<String> answers) {
		this.answers = answers;
	}

	public AnswersReader(String path) {
		answersPath = path;
		answers = new ArrayList<>();
	}

	public String getProperAnswersPath() {
		return answersPath;
	}

	public List<String> loadAnswersFromFile() {
		BufferedReader br = null;
		String line = "";
		try {

			br = new BufferedReader(new FileReader(answersPath));
			while ((line = br.readLine()) != null) {

				answers.add(line);
			}

		} catch (FileNotFoundException e) {
			return null;
		} catch (IOException e) {
			return null;
		} catch (Exception e) {
			return null;

		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					return null;
				}
			}
		}

		return answers;

	}

}
