package pl.lodz.uni.math.tdd.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class StudentsAnswersSummaryGenerator {

	private String properAnswersPath;
	private String studentsAnswersFolder;

	public StudentsAnswersSummaryGenerator(String path,
			String properAnswersPath) {
		studentsAnswersFolder = path;
		this.properAnswersPath = properAnswersPath;
	}

	public String getStudentsAnswersFolder() {
		return studentsAnswersFolder;
	}

	public Boolean generateSummaryFile(String resultsName) {
		String output = "";
		int worstResult = Integer.MAX_VALUE;
		String worstResultsIDs = "";
		int bestResult = Integer.MIN_VALUE;
		String bestResultIds = "";
		AnswersReader answersReader;
		ArrayList<String> properAnswers = (ArrayList<String>) new AnswersReader(
				properAnswersPath).loadAnswersFromFile();
		ArrayList<String> particularStudentAnswers;
		try {

			File folder = new File(studentsAnswersFolder);
			File[] listOfFiles = folder.listFiles();
			int studentsResultCounter;
			for (int i = 0; i < listOfFiles.length; i++) {
				studentsResultCounter = 0;
				if (listOfFiles[i].isFile()) {
					answersReader = new AnswersReader(studentsAnswersFolder
							+ "\\" + listOfFiles[i].getName());
					particularStudentAnswers = (ArrayList<String>) answersReader
							.loadAnswersFromFile();

					for (int j = 0; j < properAnswers.size(); j++) {
						// comparing answers
						if (properAnswers.get(j).equals(
								particularStudentAnswers.get(j))) {
							studentsResultCounter++;
						}

					}

				}
				if (i != 0) {
					output += "\n";
				}
				String studentID = listOfFiles[i].getName().substring(0,
						listOfFiles[i].getName().length() - 4);
				output += studentID + ";" + studentsResultCounter;
				if (studentsResultCounter > bestResult) {
					bestResult = studentsResultCounter;
					bestResultIds = studentID;
				} else if (studentsResultCounter == bestResult) {
					bestResultIds += ";" + studentID;
				} else if (studentsResultCounter < worstResult) {
					worstResult = studentsResultCounter;
					worstResultsIDs = studentID;
				} else if (studentsResultCounter == worstResult) {
					worstResultsIDs += ";" + studentID;
				}

			}
		} catch (Exception e) {
			return false;
		}
		PrintWriter out;
		try {
			out = new PrintWriter(resultsName + ".csv");
		} catch (FileNotFoundException e) {
			return false;
		}
		out.println(output);
		out.close();

		try {
			out = new PrintWriter("theBestAndTheWorst.csv");
		} catch (FileNotFoundException e) {
			return false;
		}
		/*
		 * Format pliku: 1 linia najlepsi studenci 2 linia najlepszy wynik 3
		 * linia najgorsi studenci 4 linia najgorszy wynik
		 */
		out.println(bestResultIds + "\n" + bestResult + "\n" + worstResultsIDs
				+ "\n" + worstResult);
		out.close();

		return true;
	}
}
