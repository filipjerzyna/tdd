package pl.lodz.uni.math.tdd.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Statistics {
	private List<Double> results;
	private SummaryResultsReader reader;

	public List<Double> getResults() {
		return results;
	}

	public Statistics(String resultsFileName) {
		reader = new SummaryResultsReader(resultsFileName);
		results = new ArrayList<Double>();
	}


	public Double calculateMeanValue() {
		if (results.size() == 0) {
			return 0.0;
		}

		Double sum = 0.0;
		for (Double result : results) {
			sum += result;
		}

		return sum / Double.valueOf(results.size());
	}

	public Double calculateStandardDeviation() {

		if (results.size() == 0) {
			return 0.0;
		}
		Double average = calculateMeanValue();
		Double sum = new Double(0);

		for (Double variable : results) {
			sum += Math.pow(variable - average, 2);
		}

		return Math.sqrt(sum / results.size());
	}

	public void synchronizeWithFile() {
		results.clear();
		List<Double> readerResults = reader.readResults();

		if (readerResults != null) {
			results = readerResults;
		}

	}

}
