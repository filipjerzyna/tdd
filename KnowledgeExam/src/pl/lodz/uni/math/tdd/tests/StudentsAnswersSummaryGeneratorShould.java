package pl.lodz.uni.math.tdd.tests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import pl.lodz.uni.math.tdd.core.StudentsAnswersSummaryGenerator;

public class StudentsAnswersSummaryGeneratorShould {

	private StudentsAnswersSummaryGenerator studentsAnswersSummaryCalculator;

	@Before
	public void setUp() {
		studentsAnswersSummaryCalculator = new StudentsAnswersSummaryGenerator(
				"studentsAnswers", "properAnswers.csv");
	}

	@Test
	public void supportWrongDirectoryPath() {
		studentsAnswersSummaryCalculator = new StudentsAnswersSummaryGenerator(
				"studentsAnswers1234", "properAnswers.csv");
		assertEquals(false,
				studentsAnswersSummaryCalculator
						.generateSummaryFile("myOutput"));
	}

	@Test
	public void generateStudentsSummaryResults() {

		String generatedResultsName = "generatedTestResults";
		studentsAnswersSummaryCalculator
				.generateSummaryFile(generatedResultsName);
		File f = new File(generatedResultsName + ".csv");

		assertEquals(true, f.exists());
	}

	@Test
	public void generateFileWithTheBestAndTheWorstStudents() {

		String generatedResultsName = "generatedTestResults";
		studentsAnswersSummaryCalculator
				.generateSummaryFile(generatedResultsName);
		File f = new File("theBestAndTheWorst" + ".csv");
		assertEquals(true, f.exists());
	}

	@Test
	public void generateProperFileWithTheBestAndTheWorstStudents() {

		String generatedResultsName = "generatedTestResults";
		studentsAnswersSummaryCalculator
				.generateSummaryFile(generatedResultsName);

		try {
			List<String> generatedResultsList = Files.readAllLines(
					Paths.get("theBestAndTheWorst.csv"),
					Charset.defaultCharset());
			assertEquals(4, generatedResultsList.size());
			assertEquals("student1", generatedResultsList.get(0));
			assertEquals("4", generatedResultsList.get(1));
			assertEquals("student2;student3", generatedResultsList.get(2));
			assertEquals("3", generatedResultsList.get(3));

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void generateProperStudentsSummaryResults() {
		String generatedResultsName = "generatedTestResults";
		studentsAnswersSummaryCalculator
				.generateSummaryFile(generatedResultsName);
		try {
			List<String> generatedResultsList = Files.readAllLines(
					Paths.get(generatedResultsName + ".csv"),
					Charset.defaultCharset());
			assertEquals(3, generatedResultsList.size());
			assertEquals("student1;4", generatedResultsList.get(0));
			assertEquals("student2;3", generatedResultsList.get(1));
			assertEquals("student3;3", generatedResultsList.get(2));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
