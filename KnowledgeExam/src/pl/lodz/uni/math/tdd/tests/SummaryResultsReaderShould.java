package pl.lodz.uni.math.tdd.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.AssertionFailedError;

import org.junit.Before;
import org.junit.Test;

import pl.lodz.uni.math.tdd.core.SummaryResultsReader;

public class SummaryResultsReaderShould {
	private SummaryResultsReader reader;

	@Before
	public void setUp() throws Exception {
		reader = new SummaryResultsReader("resultsForTest.csv");
	}

	@Test
	public void checkIfFileExists() {
		assertEquals(true, reader.fileExists());
	}

	@Test
	public void returnListWithResults() {

		List<Double> expectedResults = new ArrayList<>();
		expectedResults.add(new Double("55"));
		expectedResults.add(new Double("59"));
		expectedResults.add(new Double("14"));
		expectedResults.add(new Double("5"));
		expectedResults.add(new Double("33"));
		expectedResults.add(new Double("48"));

		assertEquals(expectedResults, reader.readResults());
	}

	@Test
	public void returnNullForWrongFileName() {
		reader.setFile("nonExistingFile.bbb");
		assertEquals(null, reader.readResults());

	}

}
