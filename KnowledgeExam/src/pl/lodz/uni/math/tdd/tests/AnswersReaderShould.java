package pl.lodz.uni.math.tdd.tests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import pl.lodz.uni.math.tdd.core.AnswersReader;

public class AnswersReaderShould {

	private AnswersReader properAnswers;

	@Before
	public void setUp() throws Exception {
		properAnswers = new AnswersReader("properAnswers.csv");
	}

	@Test
	public void loadAnswersFromFileSuccessfully() {
		List<String> expectedResults=new ArrayList<>();
		expectedResults.add("a");
		expectedResults.add("b");
		expectedResults.add("c");
		expectedResults.add("a");
		expectedResults.add("b");
		expectedResults.add("c");
		expectedResults.add("a");
		expectedResults.add("b");
		expectedResults.add("c");
		expectedResults.add("a");
		assertEquals(expectedResults, properAnswers.loadAnswersFromFile());
		
	}
	
	@Test
	public void returnNullForWrongFileName() {
		properAnswers.setAnswersPath("nonExistingFile.bbb");
		assertEquals(null, properAnswers.loadAnswersFromFile());

	}
	
	
	

}
