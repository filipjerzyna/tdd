package pl.lodz.uni.math.tdd.tests;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import pl.lodz.uni.math.tdd.core.Statistics;

public class StatisticsShould {
	private Statistics statistics;

	@Before
	public void setUp() throws Exception {

		statistics = new Statistics("resultsForTest.csv");
	}

	@Test
	public void supportWrongPath() {
		statistics = new Statistics("nonExistingResults.bbb");
		assertEquals(0, statistics.getResults().size());
	}

	@Test
	public void returnZeroForEmptyListForMeanValue() {
		assertEquals(new Double(0.0), statistics.calculateMeanValue());
	}

	@Test
	public void returnMeanValueForSimpleList() {
		statistics.getResults().add((double) 1);
		statistics.getResults().add((double) 2);
		assertEquals(new Double(1.5), statistics.calculateMeanValue());
	}

	@Test
	public void returnMeanValueForListFromFile() {
		statistics.synchronizeWithFile();
		BigDecimal result = new BigDecimal(statistics.calculateMeanValue())
				.setScale(1, RoundingMode.CEILING);
		BigDecimal expectedValue = new BigDecimal(35.6666666667).setScale(1,
				RoundingMode.CEILING);
		assertEquals(expectedValue, result);
	}

	@Test
	public void returnMeanValueForSimpleList2() {
		for (int i = 0; i < 10; i++) {
			statistics.getResults().add((double) i);
		}

		assertEquals(new Double(4.5), statistics.calculateMeanValue());
	}

	@Test
	public void returnZeroForEmptyListForStandardDeviation() {
		assertEquals(new Double(0.0), statistics.calculateStandardDeviation());
	}

	@Test
	public void returnStandardDeviationForSimpleList() {
		statistics.getResults().add((double) 2);
		statistics.getResults().add((double) 4);
		statistics.getResults().add((double) 4);
		statistics.getResults().add((double) 4);
		statistics.getResults().add((double) 5);
		statistics.getResults().add((double) 5);
		statistics.getResults().add((double) 7);
		statistics.getResults().add((double) 9);

		assertEquals(new Double(2.0), statistics.calculateStandardDeviation());
	}

	@Test
	public void synchronizeWithFileResults() {
		List<Double> expectedResults = new ArrayList<>();
		expectedResults.add(new Double("55"));
		expectedResults.add(new Double("59"));
		expectedResults.add(new Double("14"));
		expectedResults.add(new Double("5"));
		expectedResults.add(new Double("33"));
		expectedResults.add(new Double("48"));
		// Collections.sort(expectedResults);
		statistics.synchronizeWithFile();
		assertEquals(expectedResults, statistics.getResults());
	}
}
